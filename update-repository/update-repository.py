#!/usr/bin/env python3

"""Release a new version of a Kodi repository add-on

Actions that will be performed:
 - Create a new zip of the repository add-on if there is a new version
 - Download the zip of all the add-on versions
 - Update the assets of each add-on
 - Update addons.xml
 - Generate addons.xml.hash
"""

import hashlib
from pathlib import Path, PurePath
import re
import shutil
import xml.dom.minidom
import xml.etree.ElementTree as ET
import zipfile

import requests
import semver
import yaml


def release_repository_addon(addon_name):
    """Create the archive of the repository add-on

    If no zip exists with the version from addon.xml, a new zip will be
    created.
    All the other zip files will be removed so that only 1 version of the
    repository is kept."""

    print("\n#### Releasing a new version of the repository add-on ####")

    # Read the "addon.xml" file from the repository add-on
    addon_xml = ET.parse(
        Path(addon_name) / "addon.xml").getroot()
    # Get the version of the add-on (do not read the name of the add-on in the
    # XML: we assume the name of the add-on is the same as in the YAML file)
    addon_version = addon_xml.attrib["version"]

    # Define the information of the archive
    archive_path = Path(addon_name, f"{addon_name}-{addon_version}.zip")
    files_to_zip = ["addon.xml", "icon.png", "LICENSE.txt"]

    # Check if the archive for this version already exists
    if archive_path.is_file():
        print(f"An archive for the version {addon_version} of "
              f"respository.imago already exists.")
        print("The creation of the archive will be skipped for this version.")
    else:
        # Delete all the previous archives because only 1 version will be kept
        # for the repository add-on.
        for archive in Path(addon_name).glob("*.zip"):
            archive.unlink()

        # Create the archive
        with zipfile.ZipFile(file=archive_path,
                             mode="w",
                             compression=zipfile.ZIP_DEFLATED) as zip_file:
            # Add all the files to the archive
            for file in files_to_zip:
                zip_file.write(f"{addon_name}/{file}")
            # Close the archive properly
            zip_file.close()

        print(f"Archive {archive_path} successfully created!")

def update_addons(addons_info):
    """Update the add-ons with the versions configured in the YAML

    A folder will be created for each add-on. In this folder, only the archives
    of the versions configured in the YAML will remain: the other existing
    versions will be removed and the missing versions will be downloaded from
    GitLab or GitHub release page.
    Finally the assets of each add-on will be extracted from the latest version
    of the archive.
    """

    print("\n#### Updating the add-ons ####")

    for addon_info in addons_info:

        addon_name = addon_info["name"]

        if addon_info["versions"] is None:
            print(f"WARNING: Skipping the add-on {addon_name} because no"
                  f" version is configured.")
            continue
        versions = addon_info["versions"].split()

        print(f"Updating the add-on {addon_name} with version(s)"
              f" {', '.join(versions)}")

        # Create the folder for this add-on if it does not exist
        Path(addon_name).mkdir(exist_ok=True)

        # Keep only the archives of configured version and return the versions
        # which are missing.
        missing_versions = clean_up_addons(addon_name, versions)

        # Download the missing versions
        download_addons_versions(addon_name,
                                 addon_info['url'],
                                 missing_versions)

        # Extract the assets from the latest version of the add-on
        latest_version = max(
            [semver.VersionInfo.parse(ver) for ver in versions])
        extract_assets(addon_name, latest_version)

def clean_up_addons(addon_name, versions):
    """Clean up the existing files in the folder of an add-on.

    All the files will be removed except the archives matching the
    versions configured in the YAML file.
    """
    # At first assume all the versions are missing (the list must be copied
    # otherwise when a version is deleted, it will also be deleted in the
    # original list)
    missing_versions = versions.copy()

    for file in Path(addon_name).glob("*"):
        match = re.match(rf"{addon_name}-(\d\.\d\.\d)\.zip", str(file.name))
        if match:
            version = match.group(1)
            if version in versions:
                print(f"The version {version} already exists.")
                # If the version already exists in the folder, remove this
                # value from the list of missing versions.
                missing_versions.remove(version)
            else:
                print(f"The version {version} will be removed.")
                file.unlink()
        else:
            if file.is_file():
                file.unlink()
            else:
                # pathlib.rmdir() can remove only empty folders so first
                # remove all the files inside and then remove the folder
                # (we assume there cannot be any subfolder inside)
                for subfile in file.glob("*"):
                    subfile.unlink()
                file.rmdir()

    return missing_versions

def download_addons_versions(addon_name, addon_url, versions):
    """Download specific versions of an add-on

    Download the archive of specific versions of an add-on from GitLab or
    GitHub release page.
    """

    for version in versions:
        print(f"Downloading the version {version}")
        # Create the name of the file that will be stored in the repository
        file_name = f"{addon_name}-{version}.zip"
        # Build the URL to download the file: the archive will be downloaded
        # from GitHub or GitLab release page (depending on the URL)
        if "github.com" in addon_url:
            url = f"{addon_url}/archive/refs/tags/{version}.zip"
        else:
            url = f"{addon_url}/-/archive/{version}/{file_name}"
        # Get the file
        response = requests.get(url)
        # Save the file
        with open(f"{addon_name}/{file_name}", "wb") as file:
            file.write(response.content)

def extract_assets(addon_name, version):
    """Extract the assets from a specific version of an add-on archive

    Get the list of all the assets listed in the addon.xml of an add-on archive
    and extract them in the folder of the add-on.
    """

    archive_name = f"{addon_name}-{version}.zip"
    print(f"Extracting assets from {archive_name}")

    archive_path = Path(addon_name) / archive_name
    with zipfile.ZipFile(archive_path, 'r') as archive:
        # Find the exact path of addon.xml in the archive
        for file in archive.namelist():
            if "addon.xml" in file:
                addon_xml_path = file
                break
        else:
            print(f"ERROR: No assets will be extracted because the file"
                  f" addon.xml could not be found in {archive_path}")
            return

        # Get the list of all the assets from addon.xml
        with archive.open(addon_xml_path) as xml_file:
            addon_xml = ET.parse(xml_file).getroot()
            assets = addon_xml.find('.//extension/'
                                    '[@point="xbmc.addon.metadata"]/'
                                    'assets')
        # Exit if there are no assets
        if assets is None:
            print(f"WARNING: No assets found in {addon_xml_path}")
            return
        # Otherwise xtract the assets from the archive. We don't use
        # zipfile.extract() because it extracts files with their full path
        for asset in assets:
            # The path of the asset in addon.xml must be prefixed with the
            # name of the folder containing the files in the archive. This
            # prefix is extracted from the path of addon.xml to avoid
            # parsing the list of files several times.
            asset_path = str(PurePath(addon_xml_path).parent / asset.text)
            asset_in_archive = archive.open(asset_path)
            # The file must be extracted in the add-on folder with the same
            # path as in addon.xml
            target_file = Path(addon_name) / asset.text
            # If the asset will be stored in subfolder(s), they must be
            # created if they don't exist yet.
            target_file.parent.mkdir(parents=True, exist_ok=True)
            # Finally open the target file and copy the content inside to
            # actually extract the file.
            target = open(target_file, "wb")
            with asset_in_archive, target:
                shutil.copyfileobj(asset_in_archive, target)

def create_addons_xml(addons):
    """Create the file addons.xml

    This file contains the content of the addon.xml file for all the add-ons
    versions existing in the repository. The content of addon.xml is read from
    the archives even for the repository in order to have simpler code (even
    though the file could have been read  directly from the repository folder).
    """

    output_file = "addons.xml"
    print(f"\n#### Creating {output_file} ####")

    # Create the root tag called "addons" of the XML object that will be used
    root = ET.Element("addons")
    # The next subelement should be added on a new line
    root.text="\n"

    for addon in addons:
        # Find the zip files in each add-on folder
        for zip_file in Path(addon).glob("*.zip"):
            with zipfile.ZipFile(zip_file, 'r') as archive:
                # Find the exact path of addon.xml in the archive
                for file in archive.namelist():
                    if "addon.xml" in file:
                        addon_xml_path = file
                        break
                else:
                    print(f"WARNING: The version {zip_file} will not be"
                          f" referenced because the file addon.xml could not"
                          f" be found.")
                    continue

                # Add the content of addon.xml to the XML object
                with archive.open(addon_xml_path) as xml_file:
                    addon_xml = ET.parse(xml_file).getroot()
                    # Separate each add-on information with a blank line
                    addon_xml.tail = "\n\n"
                    root.append(addon_xml)

    # Convert the XML object to a string to be able to indent it with minidom
    xml_string = ET.tostring(element=root, encoding="utf-8")
    dom = xml.dom.minidom.parseString(xml_string)
    # Prettify the string with 2 spaces as indentation and no blank line
    # between each line (the first "addons" tag will be on the same line as the
    # XML declaration unfortunately...)
    pretty_xml = dom.toprettyxml(indent="  ", newl="", encoding="utf-8")
    with open(output_file, "w") as file:
        # pretty_xml is a bytes string because an encoding was provided in
        # "toprettyxml()" so it must be decoded before it is passed to open()
        file.write(pretty_xml.decode("utf-8"))

    print(f"File {output_file} created successfully.")

def generate_hash_file():
    """Generate the file addons.xml.hash"""

    hash_file_name = "addons.xml.hash"
    print(f"\n#### Generating {hash_file_name} ####")

    hash_value = hashlib.sha256()

    # Hash the content of the addons file
    with open("addons.xml", "rb") as file:
        hash_value.update(file.read())

    # Write in the hash file the hashed value
    with open(hash_file_name, "w") as file:
        file.write(hash_value.hexdigest())

    print(f"File {hash_file_name} created successfully.")

if __name__ == "__main__":

    # Read the configuration file named "config.yaml" in the current folder
    with open(Path() / "config.yaml", "r") as config_file:
        config = yaml.safe_load(config_file)

    repository_name = config["repository"]["name"]
    configured_addons = config["addons"]

    # Create a new version of the repository add-on (if it was modified)
    release_repository_addon(repository_name)

    # Update the versions of each add-ons
    update_addons(configured_addons)

    # Create the file addons.xml based on all the add-ons (including the
    # repository add-on)
    all_addons = [addon["name"] for addon in configured_addons]
    all_addons.append(repository_name)
    create_addons_xml(all_addons)

    # Update the hash value of addons.xml
    generate_hash_file()
