This repository contains utility scripts the can be used when developing Kodi
add-ons on GitLab.

[[_TOC_]]

# Create a new add-on release

This script can be used to simplify the release of a new version of an add-on.

## Prerequisites

This script requires at least Python 3.6.  
To install the required libraries, run:  
`python3 -m pip install -r create-new-add-on-release/requirements.txt`

When using the local mode with the argument `--only-archive` Git is also
required.

## Usage

For usage examples and to know the available features please run:  
`python3 create-new-add-on-release/create-new-add-on-release.py --help`

This script can be integrated in GitLab CI with the following code:
```
<job_name>:
  stage: <stage_name>
  before_script:
  - apt-get update > /dev/null
  - apt-get install --yes python3-dev python3-pip git > /dev/null
  - git clone https://framagit.org/thombet/scripts-for-kodi-add-ons.git
  - python3 -m pip --quiet install -r scripts-for-kodi-add-ons/create-new-add-on-release/requirements.txt
  script:
    - python3 scripts-for-kodi-add-ons/create-new-add-on-release/create-new-add-on-release.py
```

To be able to use this code you need to define a CI/CD variable called
`GITLAB_ACCESS_TOKEN` which value is an access token with at least the `api`
scope.

# Update a repository add-on

This script updates a repository add-on based on the content of a configuration
YAML file.

## Prerequisites

This script requires at least Python 3.6.  
To install the required libraries, run:  
`python3 -m pip install -r update-repository/requirements.txt`

## Usage

First create a file named `config.yaml` at the root of your repository.  
The format is the following:
```
repository:
 name: repository.xxxx    # name of the repository add-on (as defined in the
                          # addon.xml file of this add-on)
addons:
 - name: plugin.xxxxx     # name of the add-on (as defined in the addon.xml
                          # file of this add-on)
   url: https://xxxx      # URL of the repository containing the code of the add-on
   versions:
    X.Y.Z
    A.B.C
 - name: plugin.xxxxx     # name of the add-on (as defined in the addon.xml 
                          # file of this add-on)
   url: https://xxxx      # GitLab URL of the add-on
   versions:
    X.Y.Z
...
```

The value `addons.url` must be the URL of the repository containing the code of
the add-on (only GitLab and GitHub are supported). It will be used to download
from the release page of this code repository the .zip archive of each version
of the add-ons. Consequently a release with the name of each `addons.versions`
must exist.

The structure of the repository must be:
```
.
├── addons.xml
├── addons.xml.hash
├── config.yaml          <-- Config file read by the script
├── repository.xxxx      <-- Folder containing the repository add-on
│   ├── addon.xml
│   ├── icon.png
│   └── LICENSE.txt
├── plugin.xxxx          <-- folder of an add-on listed in config.yaml: this
│   │                        folder is optional as it will be created and
│   │                        updated by the script
│   └── ...
└── ...
```

Then run the script from the root of your repository e.g.  
`python3 update-repository/update-repository.py`

Finally commit the changes done by the script.
