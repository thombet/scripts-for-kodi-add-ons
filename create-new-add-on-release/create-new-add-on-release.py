#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
    Script for creating a new release of the add-on.

    Copyright (C) 2020 Thomas Bétous

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import argparse
import os
from pathlib import Path
import re
import subprocess
import sys
import textwrap
import xml.etree.ElementTree as ET
import zipfile

import requests


class Release():
    """Class to create a release in GitLab."""

    def __init__(self, arguments):

        # Is dry run mode enabled?
        self.dry_run = args.dry_run

        # Assume this script is called from the add-on's folder
        self.addon_path = Path().resolve()

        # Extract the information of the release from addon.xml
        self.release_info = self.build_release_information()

        # Build the common part of the REST APIs URL that will be reused for
        # all the requests (api_url does NOT have a trailing slash)
        self.API_URL = f"{arguments.api_url}/projects/{arguments.prj_id}"

        # Create the header containing the access token that will be used when
        # calling any REST API
        self.header = {"Private-Token": arguments.token}

        # Store the SHA-1 of the version the release will be created from
        self.commit_id = arguments.sha

    def _request(self, **kwargs):
        """Wrapper for the requests.request function

        The request will be sent only if the dry run mode is disabled or if it
        is a GET request (we assume GET requests can safely be run in dry run
        mode). Otherwise only information about the request will be printed.
        """

        if kwargs['method'] == "GET" or not self.dry_run:
            response = requests.request(**kwargs)

            # Raise an exception if the request didn't succeed
            response.raise_for_status()

        else:
            message = f"[Dry Run Mode] A {kwargs['method']} request would"\
                      f" have been sent to {kwargs['url']} with the header"\
                      f" {kwargs['headers']}"
            if "json" in kwargs:
                message += f" and the data:\n{kwargs['json']}"
            print(f"\n{message}\n")

            # Return None in dry run mode
            response = None

        return response

    def build_release_information(self):
        """Return a dict with the information about the release.

        The information are extracted from addon.xml to avoid mistakes when
        rewriting the information.
        """

        # Read the information of the add-on in "addon.xml" and create a dict
        addon_xml = ET.parse(self.addon_path / "addon.xml").getroot()

        # Extract the changelog and the source code URL which are in the
        # metadata node.
        metadata = {}
        for metadata_node in ["news", "source"]:
            try:
                metadata[metadata_node] = addon_xml.find(
                    f'.//extension/'
                    f'[@point="xbmc.addon.metadata"]/'
                    f'{metadata_node}').text
            except AttributeError:
                 print(f"Could not find the node {metadata_node} in the"
                       f"metadata part of the addon.xml file.\n Aborting...")

        return {
            "addon_name": addon_xml.attrib["id"],
            "version": addon_xml.attrib["version"],
            "changelog": f"# Release Notes\n\n{metadata['news']}",
            "prj_url": metadata["source"]
        }

    def release_already_exists(self):
        """Check if a release with the same version does not arleady exist.

        If it does, an exception will be raised.
        """

        # Get the list of releases
        try:
            response = self._request(method="GET",
                                     url=f"{self.API_URL}/releases",
                                     headers=self.header)
        except Exception as exception:
            print("ERROR: Failed to get the list of releases from GitLab.\n")
            raise exception

        # Check if a release with the same version already exists
        for release in response.json():

            # Check the new version against the existing tags and version names
            if (release["name"] == self.release_info["version"] or
                    release["tag_name"] == self.release_info["version"]):

                # In case a release matches, raise an exception
                raise Exception(f"ERROR: a release already exists for the"
                                f" version {self.release_info['version']}")

    def create_archive(self):
        """ Generate the archive for the release

        Only the files that are tracked by Git will be added to the archive.
        In the archive, all the files must be stored in a folder whose name is
        the name of the add-on otherwise the import in Kodi will fail.
        """

        # Build the name of the archive based on the release information
        archive_path = self.addon_path / f"{self.release_info['addon_name']}-"\
                                         f"{self.release_info['version']}.zip"

        # Check if an archive with the same name already exists
        if archive_path.exists():
            raise FileExistsError(f"Cannot create the archive: a file with the"
                                  f" name {archive_path.name} already exists.")

        # Only the files that are tracked by Git will be part of the archive.
        # The list of these files is retrieved with the command "git-ls-files"
        # run from the current folder.
        p = subprocess.run(args=["git", "ls-files"],
                           check=True,
                           capture_output=True)
        FILES_TO_INCLUDE = p.stdout.decode("utf-8").strip().split("\n")

        print(f"Creating the archive '{archive_path.name}'...")

        # Open a "zipfile" object (it will actually create an empty archive)
        with zipfile.ZipFile(
                file=archive_path,
                mode="w",
                compression=zipfile.ZIP_DEFLATED) as zip_file:
            # Add to the archive all the required files. In the archive they
            # will be located in a folder with the name of the add-on otherwise
            # the import in Kodi will fail.
            for file in FILES_TO_INCLUDE:
                zip_file.write(
                    filename=file,
                    arcname=f"{self.release_info['addon_name']}/{file}")

            # Terminate correctly the archive file
            zip_file.close()

    def create_release(self):
        """Create a Release in the GitLab project.

        The archive will be added to the assets using its URL.
        """

        # Create the information of the release
        release_data = {
            "name": self.release_info["version"],
            "tag_name": self.release_info["version"],
            "description": self.release_info["changelog"],
            "ref": self.commit_id,
        }

        # Send the POST request to create the release using "json=" instead of
        # "data=" to ensure the data is correctly encoded.
        try:
            self._request(method="POST",
                          url=f"{self.API_URL}/releases",
                          headers=self.header,
                          json=release_data)
        except Exception as exception:
            print("ERROR: Failed to create the release on GitLab.\n")
            raise exception
        else:
            print(f"Release {release_data['name']} successfully created!")

    def update_badges(self):
        """Update the badges displayed on the GitLab home page"""

        # First list all the badges in the current project
        try:
            response = self._request(method="GET",
                                     url=f"{self.API_URL}/badges",
                                     headers=self.header)
        except Exception as exception:
            print("ERROR: Failed to retrieved the list of badges.\n")
            raise exception

        # Get the ID of the badge to update (whose name is "Latest release")
        badge_name = "Latest release"
        for badge in response.json():
            if badge["name"] == badge_name:
                badge_id = badge["id"]
                badge_link_url = badge["link_url"]
                badge_image_url = badge["image_url"]
                break
        else:
            print(f"WARNING: The badge with the name '{badge_name}'' could not"
                  f" be found so the badge will not be updated.")
            return

        # Only the version value will be updated in the badge image URL: we
        # retrieve the value of the current version from the badge link URL and
        # then we replace this value by the new value in the image URL.
        match = re.match(rf"{self.release_info['prj_url']}/-/releases/(.+)",
                         badge_link_url)
        if match:
            current_version = match.group(1)
        else:
            print(f"WARNING: the current version was not found in the badge"
                  f" link URL {badge_link_url}."
                  f" The badge will not be updated.")
            return

        new_badge_image_url = badge_image_url.replace(current_version,
            self.release_info["version"])

        # Build the updated information of the badge with the new version:
        #  - link to the release
        #  - image with the version
        new_badge_data = {
            "link_url": f"{self.release_info['prj_url']}/-/releases/"
                        f"{self.release_info['version']}",
            "image_url": new_badge_image_url,
        }

        # Send the request to update the badge
        try:
            response = self._request(method="PUT",
                                     url=f"{self.API_URL}/badges/{badge_id}",
                                     headers=self.header,
                                     json=new_badge_data)
        except Exception as exception:
            print("ERROR: Failed to update the badge on GitLab.\n")
            raise exception
        else:
            print(f"Badge '{badge_name}' successfully updated!")

if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description=textwrap.dedent("""
        Create a new release of an add-on locally or in GitLab.

        This script can be used in 2 ways:
        1. with the argument "--only-archive" it will only generate an archive
           containing all the files that are tracked by Git
        OR
        2. without this argument it will create a new release in the GitLab
           repository of the add-on i.e.:
           - create a new release item in GitLab with the release notes
             extracted from addon.xml
           - update the badge named "Latest release" of the GitLab repository

        The version number, the changelog and the source code URL are extracted
        from 'addon.xml'.
        The changelog will be used as release notes in GitLab so it should be
        written in Markdown.
        """),
        epilog=textwrap.dedent("""
        == EXAMPLES ==

        Example usage in the GitLab CI:
        python3 create-new-add-on-release.py

        (a CI/CD variable called `GITLAB_ACCESS_TOKEN` is required to run the
        script without any argument. Its value must be an access token with at
        least the `api` scope.)


        Local usage:
          - Only create the archive of the add-on
        python3 create-new-add-on-release.py --only-archive
          - Simulate the creation of the release in GitLab in dry run mode
        python3 create-new-add-on-release.py --dry-run --prj-id 00000 \
--api-url https://framagit.org/api/v4 --token MyTOKen-v4lue \
--sha a1b2c3d4
        """),
        formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("--api-url", default=os.environ.get("CI_API_V4_URL"),
                        help="base URL of the REST APIs to use. The default "
                             "value is the environment variable CI_API_V4_URL")
    parser.add_argument("--dry-run", action="store_true",
                        help="Enable the dry run mode")
    parser.add_argument("--only-archive", action="store_true",
                        help="create only the archive and skip all the other "
                             "steps")
    parser.add_argument("--prj-id", default=os.environ.get("CI_PROJECT_ID"),
                        help="unique ID of the project on GitLab. The default "
                        "value is the environment variable CI_PROJECT_ID")
    parser.add_argument("--prj-url", default=os.environ.get("CI_PROJECT_URL"),
                        help="URL of the project. The default value is the "
                             "environment variable CI_PROJECT_URL")
    parser.add_argument("--sha", default=os.environ.get("CI_COMMIT_SHA"),
                        help="full commit ID of the version used for the "
                             "release. The default value is the environment "
                             "variable CI_COMMIT_SHA")
    parser.add_argument("--token",
                        default=os.environ.get("GITLAB_ACCESS_TOKEN"),
                        help="access token that will be used to authenticate. "
                        "The default value is the environment variable "
                        "GITLAB_ACCESS_TOKEN")

    args = parser.parse_args()

    # First of all check if minimum required version of python is installed.
    # This requirement is mainly for f-strings.
    if sys.version < "3.6":
        print("Error: Python >= 3.6 is required")
        sys.exit(1)

    # If the user requested the full creation of the release (and not only the
    # creation of the archive):
    if not args.only_archive:
        # Check if all the arguments were correctly set: an error will be
        # thrown in case an argument was not used AND the associated
        # environment variable doesn't exist
        error_msg = ("ERROR: {} is not set! You must use the associated "
                     "argument or define the associated environment variable.")
        wrong_arguments = False
        if args.api_url is None:
            print(error_msg.format("API_URL"))
            wrong_arguments = True
        if args.prj_id is None:
            print(error_msg.format("PRJ_ID"))
            wrong_arguments = True
        if args.token is None:
            print(error_msg.format("TOKEN"))
            wrong_arguments = True
        if args.sha is None:
            print(error_msg.format("SHA"))
            wrong_arguments = True
        if wrong_arguments:
            print("\nRead usage for more information:\n")
            parser.print_help()
            sys.exit(1)

    # Initialize the Release object: it will retrieve the release information
    # like the version from the addon.xml file of the add-on
    my_release = Release(args)

    if args.only_archive:
        # The user asked to only create the archive from the local folder.

        # First check if Git is installed (it is required to include in the
        # archive only the files that are tracked by Git)
        process = subprocess.run(args=["git", "--version"],
                                 stdout=subprocess.DEVNULL,
                                 stderr=subprocess.DEVNULL)
        if process.returncode != 0:
            print("Error: Git does not seem to be installed.")
            sys.exit(1)

        # Create the archive containing the current version of the add-on
        my_release.create_archive()
    else:
        # Check if a release with the same version does not not already exist
        my_release.release_already_exists()

        # Create the release
        my_release.create_release()

        # Update the badges
        my_release.update_badges()
